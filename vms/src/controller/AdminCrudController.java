package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.IAdminDao;
import dao.IAdminImpl;
import model.VenueDetails;

@WebServlet(name = "AdminCrudController", urlPatterns = { "/admincrud", "/home", "/delete", "/insert", "/signout",
		"/update" })
public class AdminCrudController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String url = request.getServletPath();
		IAdminDao dao = new IAdminImpl();
		
		if (url.equals("/admincrud")) {
			List<VenueDetails> list = dao.viewAll();
			request.setAttribute("vms", list);
			request.getRequestDispatcher("admincrud.jsp").include(request, response);
		} else if (url.equals("/home")) {
			List<VenueDetails> list = dao.viewAll();
			request.setAttribute("vms", list);
			request.getRequestDispatcher("admin.jsp").forward(request, response);

		} else if (url.equals("/delete")) {
			int vid = Integer.parseInt(request.getParameter("vid"));
			VenueDetails details = new VenueDetails();
			details.setVid(vid);
			dao.delete(details);
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");
			out.print(vid + " deleted Successfully <br/>");
			List<VenueDetails> list = dao.viewAll();
			request.setAttribute("vms", list);
			request.getRequestDispatcher("admincrud.jsp").forward(request, response);

		} else if (url.equals("/insert")) {
			request.getRequestDispatcher("insert.jsp").forward(request, response);
		} else if (url.equals("/signout")) {
			HttpSession session=request.getSession(false);
			String uid=(String) session.getAttribute("uid");
			System.out.println(uid +" logged out @"+new Date());
			session.invalidate();
			request.setAttribute("uid",uid+" logged out succesfully!!!");
			request.getRequestDispatcher("index.jsp").forward(request, response);
		} else if (url.equals("/update")) {
			int vid = Integer.parseInt(request.getParameter("vid"));
			String vname = request.getParameter("vname");
			String city = request.getParameter("city");
			VenueDetails details = new VenueDetails(vid, vname, city);
			request.setAttribute("vms", details);
			request.getRequestDispatcher("update.jsp").forward(request, response);
		}
	}
}
