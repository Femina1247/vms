package controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.IAdminDao;
import dao.IAdminImpl;
import model.Admin;
import model.VenueDetails;

@WebServlet(name = "AdminController", urlPatterns = { "/login", "/add", "/updateCont" })
public class AdminController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String url = request.getServletPath();
		IAdminDao dao = new IAdminImpl();
		if (url.equals("/login")) {
			String uid = request.getParameter("uid");
			String password = request.getParameter("password");
			HttpSession session=request.getSession(true);
			session.setAttribute("uid", uid);
			System.out.println(uid+" Logged in @"+new Date(session.getCreationTime()));
			Admin admin = new Admin(uid, password);
			int result = dao.adminLogin(admin);
			if (result > 0) {
				List<VenueDetails> list = dao.viewAll();
				request.setAttribute("vms", list);
				request.getRequestDispatcher("admin.jsp").forward(request, response);
			} else {
				request.setAttribute("error", "Please check your credentials");
				request.getRequestDispatcher("index.jsp").forward(request, response);
			}

		} else if (url.equals("/add")) {
			int vid = Integer.parseInt(request.getParameter("vid"));
			String vname = request.getParameter("vname");
			String city = request.getParameter("city");
			VenueDetails details = new VenueDetails(vid, vname, city);
			int result = dao.add(details);
			if (result > 0) {
				request.setAttribute("msg", vid + " inserted successfully");
			} else {
				request.setAttribute("msg", vid + " duplicate entry");
			}
			List<VenueDetails> list = dao.viewAll();
			request.setAttribute("vms", list);
			request.getRequestDispatcher("admincrud.jsp").forward(request, response);

		} else if (url.equals("/updateCont")) {
			int vid = Integer.parseInt(request.getParameter("vid"));
			String vname = request.getParameter("vname");
			String city = request.getParameter("city");
			VenueDetails details = new VenueDetails(vid, vname, city);
			dao.update(details);
			List<VenueDetails> list = dao.viewAll();
			request.setAttribute("vms", list);
			request.getRequestDispatcher("admincrud.jsp").include(request, response);
		}
	}
}
