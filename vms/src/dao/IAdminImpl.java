package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Admin;
import model.VenueDetails;
import util.Db;
import util.Query;

public class IAdminImpl implements IAdminDao {
	int result;
	PreparedStatement pst;
	ResultSet rs;

	@Override
	public int adminLogin(Admin admin) {
		result = 0;

		try {
			pst = Db.getCon().prepareStatement(Query.adminLogin);

			pst.setString(1, admin.getUid());
			pst.setString(2, admin.getPassword());
			rs = pst.executeQuery();
			while (rs.next()) {
				result++;
			}
		} catch (ClassNotFoundException | SQLException e) {

		}
		return result;

	}

	@Override
	public List<VenueDetails> viewAll() {
		List<VenueDetails> list = new ArrayList<VenueDetails>();
		try {
			pst = Db.getCon().prepareStatement(Query.viewAll);
			rs = pst.executeQuery();
			while (rs.next()) {
				VenueDetails details = new VenueDetails(rs.getInt(1), rs.getString(2), rs.getString(3));
				list.add(details);
			}
		} catch (ClassNotFoundException | SQLException e) {

			e.printStackTrace();
		}
		return list;

	}

	@Override
	public void delete(VenueDetails details) {
		try {
			pst = Db.getCon().prepareStatement(Query.delete);
			pst.setInt(1, details.getVid());
			pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();

		}
	}

	@Override
	public void update(VenueDetails details) {
		try {
			pst = Db.getCon().prepareStatement(Query.update);
			pst.setString(1, details.getVname());
			pst.setString(2, details.getCity());
			pst.setInt(3, details.getVid());
			pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();

		}
	}

	@Override
	public int add(VenueDetails details) {
		result = 0;

		try {
			pst = Db.getCon().prepareStatement(Query.add);

			pst.setInt(1, details.getVid());
			pst.setString(2, details.getVname());
			pst.setString(3, details.getCity());
			result = pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			return result;
		}
		return result;

	}

}
