package dao;

import java.util.List;

import model.Admin;
import model.VenueDetails;

public interface IAdminDao {
	public int adminLogin(Admin admin);

	public List<VenueDetails> viewAll();

	public void delete(VenueDetails details);

	public void update(VenueDetails details);

	public int add(VenueDetails details);

}
