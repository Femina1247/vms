package model;

public class VenueDetails {
	private int vid;
	private String vname;
	private String city;

	public VenueDetails() {
		// TODO Auto-generated constructor stub
	}

	public VenueDetails(int vid, String vname, String city) {
		super();
		this.vid = vid;
		this.vname = vname;
		this.city = city;
	}

	public int getVid() {
		return vid;
	}

	public void setVid(int vid) {
		this.vid = vid;
	}

	public String getVname() {
		return vname;
	}

	public void setVname(String vname) {
		this.vname = vname;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

}
