<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin</title>
</head>
<body>
	<h1>Admin Home Page</h1>
	<p align="right">
	Hi,<%=(String) session.getAttribute("uid") %>&nbsp;&nbsp;
	<a href="admincrud" style="text-decoration:none">Admin CRUD</a>
		<a href="signout">Logout</a>
	</p>
	<hr /><br/>
	<table border="5">
		<tr>
			<th colspan="3">Venue Details<br />
			<br /></th>
		</tr>
		<tr>
			<th>Venue ID</th>
			<th>Venue Name</th>
			<th>City</th>
		</tr>
		<c:forEach items="${vms}" var="v">
			<tr style="text-align:center;">
				<td>${v.getVid()}
				<br /></td>
				<td>${v.getVname()}
				<br /></td>
				<td>${v.getCity()}
				<br /></td>
				</tr>
		</c:forEach>
	</table>
</body>
</html>