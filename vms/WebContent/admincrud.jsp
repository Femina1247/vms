<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin CRUD</title>
</head>
<body>
<h1>Admin CRUD Operations Page</h1>
	<p align="right">
	Hi,<%=(String) session.getAttribute("uid") %>&nbsp;&nbsp;
	<a href="home">Home</a>
	<a href="insert">Add new venue</a>
		<a href="signout">Logout</a>
	</p>
	<hr /><br/>
	${msg}
	<table border="5">
		<tr>
			<th colspan="5">Venue Details<br />
			<br /></th>
		</tr>
		<tr>
			<th>Venue ID</th>
			<th>Venue Name</th>
			<th>City</th>
			<th>Update</th>
			<th>Delete</th>
		</tr>
		<c:forEach items="${vms}" var="v">
			<tr style="text-align:center;">
				<td>${v.getVid()}</td>
				<td>${v.getVname()}</td>
				<td>${v.getCity()}</td>
				<td><a style="text-decoration:none" href="update?vid=${v.getVid()}&vname=${v.getVname()}&city=${v.getCity()}">Update</a></td>
				<td><a style="text-decoration:none" href="delete?vid=${v.getVid()}">Delete</a></td>
				</tr>
		</c:forEach>
	</table>

</body>
</html>